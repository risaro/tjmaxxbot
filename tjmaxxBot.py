import datetime
import os
import sys
import traceback
import logging
from http.client import RemoteDisconnected

from selenium import webdriver
from requests_html import HTMLSession
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from urllib3.exceptions import MaxRetryError, ProtocolError

base_url = 'https://tjmaxx.tjx.com/store/jump/product/'
TEST = False
SESSIONS_COUNT = 0
REQUESTS_DELAY = 0
DEFAULT_DRIVER_WAIT = 0


class Shipping:
    firstname = "Amy"
    lastName = "Quinn"
    address = "2996 Hartley Drive"
    zipCode = "37043"
    city = "Clarksville"
    state = "TN"
    phoneNumber = "9319198600"
    email = "amyquinn2021@gmail.com"


class Payment:
    creditcard = "4266902067069995"
    cvv = "732"
    experationMonth = "12"
    experationYear = "2023"


def print_t(text):
    current_time = datetime.datetime.now()
    time_str = current_time.strftime('[%d.%m.%g %T]')
    print(f'{time_str} {text}')
    logging.info(text)


def str_to_file_name(string):
    return string.replace(':', ' ').replace('-', ' ').replace('?', '').strip()


def set_value(driver, element, value):
    driver.execute_script(f'arguments[0].value = "{value}";', element)


def uncheck(driver, element):
    if element.is_selected():
        driver.execute_script(f'arguments[0].click();', element)


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=print_end)
    # Print New Line on Complete
    if iteration == total:
        print()


def get_basic_driver(executable_path=None, headless=False):
    print_t('Starting webdriver...')
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("--remote-debugging-port=9222")
    options.add_argument('log-level=3')
    options.add_argument("--incognito")
    if headless:
        options.add_argument("--headless")
    options.add_experimental_option("excludeSwitches", ['enable-automation'])
    options.add_experimental_option('useAutomationExtension', False)
    if not TEST:
        prefs = {"profile.managed_default_content_settings.images": 2}
        options.add_experimental_option("prefs", prefs)
    if executable_path:
        _driver = webdriver.Chrome(options=options, executable_path=executable_path)
    else:
        _driver = webdriver.Chrome(options=options)

    _driver.execute_cdp_cmd("Network.enable", {})
    _driver.set_window_position(0, 0)
    _driver.set_window_size(1366, 768)
    print_t('Webdriver started.')
    return _driver


def get_product_links(target_name, empty=False):
    """
    Returns list of elements "items",
    each containing a link to product detail page
    """
    base_shop = base_url
    session = HTMLSession()
    if not empty:
        r = session.get(base_shop)
    item = target_name
    return item, session


def get_matched_and_available(target_name):
    """
    Given a target name, filter the product on main page,
    and return links to products with available items

    checked_urls: if already checked (and not a match in product name),
    skip in future checks

    Exactly how this should work, depends on how the drop works - is the page already there,
    just not for sale yet? Or page is added at drop time?
    """
    potential_urls = []
    sessions = []
    session_index = 0
    print_t('Initializing sessions...')
    for i in range(0, SESSIONS_COUNT):
        print_progress_bar(i, SESSIONS_COUNT, prefix=datetime.datetime.now().strftime('[%d.%m.%g %T]'),
                           suffix='Complete', length=45)
        try:
            item, session = get_product_links(target_name, empty=True)
            sessions.append(session)
        except MaxRetryError:
            print_t('MaxRetryError, sleeping for 10s...')
            time.sleep(10)
        time.sleep(0.01)

    print_progress_bar(SESSIONS_COUNT, SESSIONS_COUNT, prefix=datetime.datetime.now().strftime('[%d.%m.%g %T]'),
                       suffix='Complete', length=45)

    # if base_url in item:
    #     target_url = item
    # else:
    #     target_url = base_url + item

    target_url = item

    print_t(target_url)

    try:
        r = sessions[0].get(target_url)
    except (ConnectionError, ProtocolError, RemoteDisconnected):
        print_t('Cannot connect to tjmaxx, retrying...')
        raise RuntimeError('Could not connect to tjmaxx')

    found = True
    if found:
        item_available = False
        print_t("Checking for item: Click CTR + C to stop search.")
        while not item_available:
            item_available = check_can_buy(r)
            if not item_available:
                try:
                    r = sessions[session_index].get(target_url)
                except (ConnectionError, ProtocolError, RemoteDisconnected) as ex:
                    print_t(f'Connection error, 5s timeout.\nError message: {str(ex)}')
                    time.sleep(5)

                session_index += 1
                if session_index == SESSIONS_COUNT - 1:
                    session_index = 0
                time.sleep(REQUESTS_DELAY)
        potential_urls.append(target_url)
    else:
        print_t(f'Not a match')

    return potential_urls


def check_can_buy(r):
    """
    Given a page (returned by session.get(target_url)),
    find if there is such html code within:
    <input type="submit" name="commit" value="add to cart" class="button">
    Returns True if so, False if not
    """
    # buy_btn = r.html.find('input[id="addItemToOrder"]', first=True)
    buy_btn = r.html.xpath('//input[contains(@value, "ADD TO BAG")]', first=True)
    if buy_btn:
        quantity_btn = r.html.find('select[class="quantity"]', first=True)
        if quantity_btn:
            return True
    return False


def perform_purchase(url, product_quantity):
    """
    Given url of product, add to cart then checkout
    """
    shipping = Shipping()
    payment = Payment()
    driver = get_basic_driver()
    try:
        is_added = False
        while not is_added:
            is_added = add_to_cart(driver, product_quantity, url)
            if not is_added:
                time.sleep(REQUESTS_DELAY)

        print_t('Added, proceeding to cart...')
        # go to checkout
        checkout_url = 'https://tjmaxx.tjx.com/store/checkout/cart.jsp'
        driver.get(checkout_url)
        time.sleep(1)

        print_t('Proceeding to checkout...')

        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.element_to_be_clickable((By.ID, 'checkout')))
        driver.find_element_by_id('checkout').click()

        print_t('Entering guest checkout...')
        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.presence_of_element_located((By.ID, 'account-modal')))
        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.element_to_be_clickable((By.ID, 'guestCheckoutBtn')))
        guest_checkout_btn = driver.find_element_by_id('guestCheckoutBtn')
        driver.execute_script('arguments[0].click();', guest_checkout_btn)

        time.sleep(1)

        # fill in form
        print_t('Filling shipping details...')
        driver.find_element_by_id('shippingfirstNameInput').send_keys(shipping.firstname)
        driver.find_element_by_id('shippinglastNameInput').send_keys(shipping.lastName)
        driver.find_element_by_id('shippingstreetAddress1Input').send_keys(shipping.address)
        driver.find_element_by_id('shippingpostalCodeInput').send_keys(shipping.zipCode)
        set_value(driver, driver.find_element_by_id('shippingcityInput'), shipping.city)

        state_select_element = Select(driver.find_element_by_id('stateSelect'))
        state_select_element.select_by_value(shipping.state)

        driver.find_element_by_id('shippingtelephoneInput1').send_keys(shipping.phoneNumber)
        driver.find_element_by_id('contact-email').send_keys(shipping.email)

        uncheck(driver, driver.find_element_by_id('signUpForEmail'))
        uncheck(driver, driver.find_element_by_id('signUpForNewArrivals'))
        # time.sleep(1)
        # driver.find_element_by_xpath('//label[@for="shippingMethod2"]').click()
        # time.sleep(1)
        driver.execute_script(
            'document.getElementById("shippingMethod2").checked = true;')
        print_t('Done, proceeding to payment data...')
        driver.find_element_by_id('shipping-details').click()
        time.sleep(1)

        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.presence_of_element_located((By.ID, 'creditCardNumber')))
        driver.find_element_by_id('creditCardNumber').send_keys(payment.creditcard)
        driver.find_element_by_id('creditCardCVV').send_keys(payment.cvv)
        driver.find_element_by_id('creditCardExpMonth').send_keys(payment.experationMonth)
        driver.find_element_by_id('creditCardExpYear').send_keys(payment.experationYear)
        driver.find_element_by_id('applyPayments').click()

        if TEST:
            print_t('Finished, sleeping for 1min now...')
            time.sleep(60)
        else:
            driver.find_element_by_id('commitOrderButton').click()

    except KeyboardInterrupt:
        print_t('CTRL+C, exiting...')
        driver.quit()
        sys.exit(0)
    except:
        screenshot_file_name = f'screenshots/webdriver_exception_{int(time.time())}.png'
        print_t(f'Webdriver exception, screenshot at {screenshot_file_name}')
        driver.save_screenshot(screenshot_file_name)
        driver.quit()
        raise


def add_to_cart(driver, product_quantity, url):
    driver.get(url)
    time.sleep(1)

    # check for campaign modal loading
    try:
        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(
            EC.element_to_be_clickable((By.XPATH, '//button[contains(@id, "bx-close")]')))
        print_t('Campaign modal found, closing...')
        campaign_close_button = driver.find_elements_by_xpath('//button[contains(@id, "bx-close")]')
        if campaign_close_button:
            campaign_close_button[0].click()

        time.sleep(2)
    except TimeoutError:
        pass

    print_t(f'Adding to cart, quantity: {product_quantity}')
    quantity_element = driver.find_element_by_class_name('quantity')
    if int(product_quantity) <= 10:
        driver.execute_script(f'arguments[0].value={product_quantity}', quantity_element)
    else:
        js_text = f'''
            let new_option = new Option("{product_quantity}", "{product_quantity}");
            arguments[0].add(new_option);
            arguments[0].value = "{product_quantity}"; 
            '''
        driver.execute_script(js_text, quantity_element)
    color_elements = driver.find_elements_by_xpath('//ul[contains(@class, "color-options-list")]/li')
    if color_elements:
        color_name = color_elements[0].find_element_by_xpath('./a/span').get_attribute('innerHTML')
        print_t(f'Color select found, selecting first available color: {color_name}')
        color_elements[0].click()
    else:
        print_t('No color select found')
    btn = driver.find_element_by_id('addItemToOrder')
    btn.click()

    try:
        WebDriverWait(driver, DEFAULT_DRIVER_WAIT).until(EC.presence_of_element_located((By.ID, 'cart-modal')))
        driver.find_element_by_xpath('//div[@class="atc-item-error"]')
        raise RuntimeError('Message: This item is no longer available and will be removed from your bag.')
    except TimeoutError:
        screenshot_file_name = f'screenshots/no_modal_{int(time.time())}.png'
        print_t(f'No cart modal, retrying. Screenshot: {screenshot_file_name}')
        driver.save_screenshot(screenshot_file_name)
        return False
    except NoSuchElementException:
        pass

    return True


def main(target_product, product_quantity):
    print_t("Started")
    urls = get_matched_and_available(target_product)
    print_t(f'Found {len(urls)} matches.')
    if len(urls) == 0:
        print_t('No match found - checking again')
        return
    print_t(f'Processing first url: {urls[0]}')
    # just buy the first match
    url = urls[0]
    for i in range(5):
        try:
            print_t(f'Attempt #{i + 1}')
            perform_purchase(url, product_quantity)
            print_t('Done.')
            return True
        except KeyboardInterrupt:
            print_t('CTRL+C, exiting...')
            sys.exit(0)
        except:
            print_t('Retrying perform_purchase()...')
            print_t('------------------STACK TRACE------------------')
            print_t(traceback.format_exc())
            print_t('----------------END STACK TRACE----------------')


# define main
if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Tjmaxx main parser')
    parser.add_argument('-n', '--name', required=True,
                        help='Specify product name to find and purchase')
    parser.add_argument('-q', '--quantity', required=True)
    parser.add_argument('-t', '--test', required=False, action='store_true')
    parser.add_argument('-s', '--sessions', required=False, default=50)
    parser.add_argument('-d', '--delay', required=False, default=1)
    parser.add_argument('-w', '--wait', required=False, default=5)

    args = parser.parse_args()

    REQUESTS_DELAY = args.delay
    SESSIONS_COUNT = args.sessions
    DEFAULT_DRIVER_WAIT = args.wait

    result = False
    attempts = 1

    current_time = datetime.datetime.now()
    time_str = current_time.strftime('%d_%m_%g_%H_%M')

    if not os.path.exists('screenshots'):
        os.makedirs('screenshots')

    if not os.path.exists('logs'):
        os.makedirs('logs')

    log_file_name = str_to_file_name(f'logs/{args.name.split("/")[-1]}_{time_str}.log')
    logging.basicConfig(filename=log_file_name, level=logging.INFO, format='[%(asctime)s][%(levelname)s] %(message)s')

    if args.test:
        TEST = True

    if TEST:
        print_t('-------------------TEST RUN--------------------')

    print_t(f'Log file: {log_file_name}')
    while not result:
        print_t(f'main(), attempt #{attempts}')
        try:
            result = main(target_product=args.name, product_quantity=args.quantity)
        except RuntimeError:
            pass
        except KeyboardInterrupt:
            print_t('CTRL+C, exiting...')
            sys.exit(0)
        except Exception as ex:
            print_t('------------------STACK TRACE------------------')
            print_t(traceback.format_exc())
            print_t('----------------END STACK TRACE----------------')
            print_t('Retrying main()')
            attempts += 1
